﻿using System.Windows;

namespace SystemyWizyjne
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            new MainWindow().Show();
        }
    }
}