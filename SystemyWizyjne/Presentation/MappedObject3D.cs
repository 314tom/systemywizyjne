﻿using System.Windows.Media.Media3D;
using SystemyWizyjne.Scene;

namespace SystemyWizyjne.Presentation
{
    public class MappedObject3D
    {
        public string Filename { get; }
        public Marker MarkerData { get; }
        public ModelVisual3D Model { get; }
        public float MarkerWidth { get; }

        public MappedObject3D(string filename, Marker marker, ModelVisual3D model, float markerWidth = 12)
        {
            Filename = filename;
            MarkerData = marker;
            Model = model;
            MarkerWidth = markerWidth;
        }

        public MappedObject3D(string filename, Marker marker, float markerWidth = 12)
        {
            Filename = filename;
            MarkerData = marker;
            MarkerWidth = markerWidth;
            Model = SceneReader.ReadScene(filename);
        }
    }
}
