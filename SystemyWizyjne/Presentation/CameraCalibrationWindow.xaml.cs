﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using MessageBox = System.Windows.MessageBox;
using Size = System.Drawing.Size;
using Point = System.Drawing.Point;

namespace SystemyWizyjne.Presentation
{
    /// <summary>
    /// Interaction logic for CameraCalibrationWindow.xaml
    /// </summary>
    public partial class CameraCalibrationWindow
    {
        readonly VideoCapture _capture;
        private Mat _frame = new Mat();
        private readonly Mat _grayFrame = new Mat();
        private int _width;
        private int _height;
        private float _squareSize;
        private Size _patternSize;
        private VectorOfPointF _corners = new VectorOfPointF();

        private Mat[] _rvecs;
        private Mat[] _tvecs;

        private static Mat[] _frameArrayBuffer;
        private int _frameBufferSavepoint;
        private bool _startFlag;
        private bool _find;

        public enum Mode
        {
            View,
            CalculatingIntrinsics,
            Calibrated,
            SavingFrames
        }

        private Mode _currentMode = Mode.View;
        
       

        private MCvPoint3D32f[][] _cornersObjectList;
        private PointF[][] _cornersPointsList;
        private VectorOfPointF[] _cornersPointsVec;

        public bool IsCalibrated => _currentMode == Mode.Calibrated;
        public Mat CameraMatrix = new Mat(3, 3, DepthType.Cv64F, 1);
        public Mat DistCoeffs = new Mat(8, 1, DepthType.Cv64F, 1);
        


        public CameraCalibrationWindow()
        {
            InitializeComponent();
            CvInvoke.UseOpenCL = false;
            try
            {
                _capture = new VideoCapture();
                _capture.ImageGrabbed += ProcessFrame;
                _capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            _capture.Retrieve(_frame);
            CvInvoke.CvtColor(_frame, _grayFrame, ColorConversion.Bgr2Gray);
            if (_currentMode == Mode.SavingFrames)
            {
                _find = CvInvoke.FindChessboardCorners(_grayFrame, _patternSize, _corners,
                    CalibCbType.AdaptiveThresh | CalibCbType.FastCheck | CalibCbType.NormalizeImage);
                if (_find)
                {
                    CvInvoke.CornerSubPix(_grayFrame, _corners, new Size(11, 11), new Size(-1, -1),
                        new MCvTermCriteria(30, 0.1));

                    if (_startFlag)
                    {
                        _frameArrayBuffer[_frameBufferSavepoint] = _grayFrame;
                        _frameBufferSavepoint++;

                        if (_frameBufferSavepoint == _frameArrayBuffer.Length)
                            _currentMode = Mode.CalculatingIntrinsics;
                    }

                    CvInvoke.DrawChessboardCorners(_frame, _patternSize, _corners, _find);
                    string msg = $"{_frameBufferSavepoint + 1}/{_frameArrayBuffer.Length}";

                    int baseLine = 0;
                    var textOrigin = new Point(_frame.Cols - 2 * 120 - 10, _frame.Rows - 2 * baseLine - 10);
                    CvInvoke.PutText(_frame, msg, textOrigin, FontFace.HersheyPlain, 3, new MCvScalar(0, 0, 255), 2);

                    Thread.Sleep(100);
                }
                _corners = new VectorOfPointF();
                _find = false;
            }
            if (_currentMode == Mode.CalculatingIntrinsics)
            {
                for (int k = 0; k < _frameArrayBuffer.Length; k++)
                {
                    _cornersPointsVec[k] = new VectorOfPointF();
                    CvInvoke.FindChessboardCorners(_frameArrayBuffer[k], _patternSize, _cornersPointsVec[k],
                        CalibCbType.AdaptiveThresh
                        | CalibCbType.FastCheck | CalibCbType.NormalizeImage);
                    CvInvoke.CornerSubPix(_grayFrame, _cornersPointsVec[k], new Size(11, 11), new Size(-1, -1),
                        new MCvTermCriteria(30, 0.1));

                    var objectList = new List<MCvPoint3D32f>();
                    for (int i = 0; i < _height; i++)
                    {
                        for (int j = 0; j < _width; j++)
                        {
                            objectList.Add(new MCvPoint3D32f(j * _squareSize, i * _squareSize, 0.0F));
                        }
                    }
                    _cornersObjectList[k] = objectList.ToArray();
                    _cornersPointsList[k] = _cornersPointsVec[k].ToArray();
                }
                CvInvoke.CalibrateCamera(_cornersObjectList, _cornersPointsList, _grayFrame.Size,
                    CameraMatrix, DistCoeffs, CalibType.RationalModel, new MCvTermCriteria(30, 0.1), out _rvecs,
                    out _tvecs);

                _currentMode = Mode.Calibrated;
            }
            if (_currentMode == Mode.Calibrated)
            {
                Mat outFrame = _frame.Clone();
                CvInvoke.Undistort(_frame, outFrame, CameraMatrix, DistCoeffs);
                _frame = outFrame.Clone();
            }

            CameraImage.Image = _frame;
        }

        private void Start_BTN_Click(object sender, EventArgs e)
        {
            _frameArrayBuffer = new Mat[int.Parse(numFrameBuffer.Text)];
            _cornersObjectList = new MCvPoint3D32f[_frameArrayBuffer.Length][];
            _cornersPointsList = new PointF[_frameArrayBuffer.Length][];
            _cornersPointsVec = new VectorOfPointF[_frameArrayBuffer.Length];
            _frameBufferSavepoint = 0;

            _width = int.Parse(numChessWidth.Text);
            _height = int.Parse(numChessHeight.Text);
            _squareSize = float.Parse(numSquareSize.Text);
            _patternSize = new Size(_width, _height);

            _startFlag = true;
            _currentMode = Mode.SavingFrames;
        }

        private void Save_BTN_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = _currentMode == Mode.Calibrated;
        }
    }
}