﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using SystemyWizyjne.Scene;
using Microsoft.Win32;

namespace SystemyWizyjne.Presentation
{
    /// <summary>
    /// Interaction logic for UserDefinedInputWindow.xaml
    /// </summary>
    public partial class MarkerAndModelCreatorWindow
    {
        private readonly List<Marker> _takenMarkers;
        private const int WidthOfRectangle = 50;
        private const int HeightOfRectangle = WidthOfRectangle;
        private const int MarginRectangle = 50;
        private const int SpaceRectangle = 1;
        private readonly int _countX;
        private readonly int _countY;
        private readonly Color _notSelectedColor = Colors.Black;
        private readonly Color _selectedColor = Colors.White;

        private List<List<bool>> Output { get; set; }

        private Marker _marker;
        private ModelVisual3D _model;

        public MappedObject3D Model => new MappedObject3D(FilenameTextInput.Text, _marker, _model);

        private List<Color> RectangleColors { get; set; }

        public MarkerAndModelCreatorWindow(int width, int height, List<Marker> markers)
        {
            _takenMarkers = markers;
            _countX = width;
            _countY = height;
            InitColors(null);
            InitializeComponent();
            InputGrid.Width = _countX * (WidthOfRectangle + SpaceRectangle) + 2 * MarginRectangle - SpaceRectangle;
            InputGrid.Height = _countY * (HeightOfRectangle + SpaceRectangle) + 2 * MarginRectangle - SpaceRectangle;
            Height = InputGrid.Height + 100;
            Width = InputGrid.Width + 50;
            ResizeMode = ResizeMode.NoResize;
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    AddRectangle(x, y);
                }
            }
            UpdateOutput();
            AcceptButton.IsEnabled = false;
        }

        private void AddRectangle(int x, int y)
        {
            var v = new Rectangle
            {
                Fill = new SolidColorBrush(RectangleColors[y * _countX + x]),
                Width = WidthOfRectangle,
                Height = HeightOfRectangle,
                RadiusX = WidthOfRectangle / 8d,
                RadiusY = HeightOfRectangle / 8d,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin =
                    new Thickness(x * (WidthOfRectangle + SpaceRectangle) + MarginRectangle,
                        y * (HeightOfRectangle + SpaceRectangle) + MarginRectangle, 0, 0)
            };
            v.MouseDown += RectangleClick;
            v.MouseEnter += RectangleMouseOver;
            InputGrid.Children.Add(v);
        }

        private void RectangleMouseOver(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.LeftButton == MouseButtonState.Pressed)
                ChangeColor((Rectangle)sender, 1);
            else if (mouseEventArgs.RightButton == MouseButtonState.Pressed)
                ChangeColor((Rectangle)sender, 2);
            else if (mouseEventArgs.MiddleButton == MouseButtonState.Pressed)
                ChangeColor((Rectangle)sender);
        }

        private void RectangleClick(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            ChangeColor((Rectangle)sender);
        }

        public void ChangeColor(Rectangle rectangle, int mode = 0)
        {
            var position = GetIndex(rectangle);
            int x = position % _countX;
            int y = position / _countX;
            Color newColor;
            switch (mode)
            {
                case 1:
                    newColor = _selectedColor;
                    Output[y][x] = true;
                    break;
                case 2:
                    newColor = _notSelectedColor;
                    Output[y][x] = false;
                    break;
                default:
                    newColor = RectangleColors[position].Equals(_notSelectedColor) ? _selectedColor : _notSelectedColor;
                    Output[y][x] = RectangleColors[position].Equals(_notSelectedColor);
                    break;
            }
            RectangleColors[position] = newColor;
            rectangle.Fill = new SolidColorBrush(newColor);
            UpdateOutput();
            UpdateSaveAllowed();
        }

        private int GetIndex(Rectangle rectangle)
        {
            int index;
            for (index = 0;
                index < InputGrid.Children.Count && !ReferenceEquals(InputGrid.Children[index], rectangle);
                index++)
            {
            }
            return index;
        }

        private void UpdateOutput()
        {
            _marker = new Marker(Output).Rotated().GetYSymmetric();
        }

        private void UpdateSaveAllowed()
        {
            AcceptButton.IsEnabled = !string.IsNullOrEmpty(FilenameTextInput.Text) &&
                                     _marker != null &&
                                     _marker.Equals(_marker.GetXSymmetric()) == -1 &&
                                     _marker.Equals(_marker.GetYSymmetric()) == -1 &&
                                     _takenMarkers.TrueForAll(m => m.Equals(_marker) == -1) &&
                                     _takenMarkers.TrueForAll(m => m.Equals(_marker.GetXSymmetric()) == -1);
        }

        private void InitColors(Marker marker)
        {
            if (marker != null)
            {
                Output = marker.Values;
                RectangleColors = new List<Color>();
                Output.ForEach(line => line.ForEach(
                    value => RectangleColors.Add(value ? _selectedColor : _notSelectedColor)));
            }
            else
            {
                RectangleColors = new List<Color>();
                Output = new List<List<bool>>();
                for (var y = 0; y < _countY; y++)
                {
                    List<bool> t = new List<bool>();
                    for (var x = 0; x < _countX; x++)
                    {
                        RectangleColors.Add(_selectedColor);
                        t.Add(true);
                    }
                    Output.Add(t);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            UpdateOutput();
            DialogResult = true;
        }

        private void Open_Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "3D files (.obj)|*.obj"
            };
            if (dialog.ShowDialog() != true) return;
            try
            {
                _model = SceneReader.ReadScene(dialog.FileName);
                FilenameTextInput.Text = dialog.FileName;
            }
            catch (Exception)
            {
                FilenameTextInput.Text = "";
                _model = null;
            }
            UpdateSaveAllowed();
        }
    }
}
