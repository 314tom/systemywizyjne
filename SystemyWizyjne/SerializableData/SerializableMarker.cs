﻿using System.Collections.Generic;

namespace SystemyWizyjne.SerializableData
{
    public class SerializableMarker
    {
        public List<List<bool>> Values { get; set; }

        public SerializableMarker()
        {
        }

        public SerializableMarker(List<List<bool>> values)
        {
            Values = values;
        }

        public SerializableMarker(Marker marker)
        {
            Values = marker.Values;
        }

        public Marker ToMarker()
        {
            return new Marker(Values);
        }
    }
}