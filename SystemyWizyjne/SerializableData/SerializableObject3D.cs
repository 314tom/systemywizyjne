﻿using SystemyWizyjne.Presentation;

namespace SystemyWizyjne.SerializableData
{
    public class SerializableObject3D
    {
        public string Filename { get; set; }
        public SerializableMarker Marker { get; set; }

        public SerializableObject3D()
        {
        }

        public SerializableObject3D(string filename, Marker markerData)
        {
            Filename = filename;
            Marker = new SerializableMarker(markerData);
        }

        public SerializableObject3D(MappedObject3D mappedObject)
        {
            Filename = mappedObject.Filename;
            Marker = new SerializableMarker(mappedObject.MarkerData);
        }

        public MappedObject3D ToMappedObject3D()
        {
            return new MappedObject3D(Filename, Marker.ToMarker());
        }
    }
}