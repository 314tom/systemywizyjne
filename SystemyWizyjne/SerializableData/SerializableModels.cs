﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using SystemyWizyjne.Presentation;

namespace SystemyWizyjne.SerializableData
{
    public class SerializableModels
    {
        public List<SerializableObject3D> Objects3D { get; set; }

        public SerializableModels()
        {
        }

        public SerializableModels(List<SerializableObject3D> objects3D)
        {
            Objects3D = objects3D;
        }

        public SerializableModels(List<MappedObject3D> objects3D)
        {
            Objects3D = objects3D.Select(o => new SerializableObject3D(o)).ToList();
        }

        public List<MappedObject3D> GetMappedObjects3D()
        {
            return Objects3D.Select(o => o.ToMappedObject3D()).ToList();
        }

        public bool SaveData(string filename)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(SerializableModels));
                StreamWriter streamWriter = new StreamWriter(filename);
                xmlSerializer.Serialize(streamWriter, this);
                streamWriter.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static SerializableModels Read(string filename)
        {
            using (StreamReader stream = new StreamReader(filename))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(SerializableModels));
                return xmlSerializer.Deserialize(stream) as SerializableModels;
            }
        }
    }
}