﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using SystemyWizyjne.MyMath;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace SystemyWizyjne
{
    public static class Extensions
    {
        public static byte GetByteValue(this Mat mat, int row, int col)
        {
            var value = CreateByteElement();
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        public static void SetByteValue(this Mat mat, int row, int col, byte value)
        {
            byte[] target = CreateByteElement(value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }

        private static byte[] CreateByteElement(byte value)
        {
            byte[] element = CreateByteElement();
            element[0] = value;
            return element;
        }

        private static byte[] CreateByteElement()
        {
            return new byte[1];
        }

        public static double GetDoubleValue(this Mat mat, int row, int col)
        {
            var value = CreateDoubleElement();
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        public static void SetDoubleValue(this Mat mat, int row, int col, double value)
        {
            double[] target = CreateDoubleElement(value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }

        private static double[] CreateDoubleElement(double value)
        {
            double[] element = CreateDoubleElement();
            element[0] = value;
            return element;
        }

        private static double[] CreateDoubleElement()
        {
            return new double[1];
        }

        public static void SetFloatValue(this Mat mat, int row, int col, float value)
        {
            float[] target = CreateFloatElement(value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }

        public static float GetFloatValue(this Mat mat, int row, int col)
        {
            var value = CreateFloatElement();
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * 4, value, 0, 1);
            return value[0];
        }

        private static float[] CreateFloatElement(float value)
        {
            float[] element = CreateFloatElement();
            element[0] = value;
            return element;
        }

        private static float[] CreateFloatElement()
        {
            return new float[1];
        }

        public static Image<TColor, TDepth> GetAxisAlignedImagePart<TColor, TDepth>(
            this Image<TColor, TDepth> input,
            MyQuadrilateral rectDst,
            Size targetImageSize)
            where TColor : struct, IColor
            where TDepth : new()
        {
            MyQuadrilateral rectSrc = new MyQuadrilateral(0, 0, targetImageSize.Width, targetImageSize.Height);
            var src = ToPointFs(new[]
                {rectSrc.GetPoint(0), rectSrc.GetPoint(1), rectSrc.GetPoint(2), rectSrc.GetPoint(3)});
            var dst = ToPointFs(new[]
                {rectDst.GetPoint(0), rectDst.GetPoint(1), rectDst.GetPoint(2), rectDst.GetPoint(3)});
            using (var matrix = CvInvoke.GetPerspectiveTransform(src, dst))
            {
                var outputArray = new Mat();
                CvInvoke.Invert(matrix, outputArray, DecompMethod.LU);

                using (var cutImagePortion = new Mat())
                {
                    CvInvoke.WarpPerspective(input, cutImagePortion, outputArray, targetImageSize, Inter.Cubic);
                    return cutImagePortion.ToImage<TColor, TDepth>();
                }
            }
        }

        public static void Print(this Mat outputArray)
        {
            Console.WriteLine(outputArray.Size);
            Console.WriteLine("--------------------------");
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    Console.Write(outputArray.GetByteValue(y, x) + ", ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("--------------------------");
        }

        private static PointF[] ToPointFs(Point[] points)
        {
            return points.ToList().Select(p => p.ToPointF()).ToArray();
        }

        public static PointF ToPointF(this Point point)
        {
            return new PointF(point.X, point.Y);
        }

        public static double Distance(this Point p1, Point p)
        {
            double diffX = p1.X - p.X;
            double diffY = p1.Y - p.Y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }

        public static bool Intersects(this LineSegment2D line1, LineSegment2D line2)
        {
            return GetIntersectionPoint(line1.P1, line1.P2, line2.P1, line2.P2);
        }

        private static bool GetIntersectionPoint(Point a1, Point a2, Point b1, Point b2)
        {
            Point r = new Point(a2.X - a1.X, a2.Y - a1.Y);
            Point s = new Point(b2.X - b1.X, b2.Y - b1.Y);
            return Cross(r, s) != 0;
        }

        private static int Cross(Point v1, Point v2)
        {
            return v1.X * v2.Y - v1.Y * v2.X;
        }

        public static bool IntersectsInside(this LineSegment2D line1, LineSegment2D line2)
        {
            Tuple<double, double, double> eq1 = line1.GetEquation();
            double a1 = eq1.Item1;
            double b1 = eq1.Item2;
            double c1 = eq1.Item3;
            Tuple<double, double, double> eq2 = line2.GetEquation();
            double a2 = eq2.Item1;
            double b2 = eq2.Item2;
            double c2 = eq2.Item3;

            double det = a1 * b2 - a2 * b1;
            if (!(Math.Abs(det) > 0)) return false;
            double x = (b2 * c1 - b1 * c2) / det;
            double y = (a1 * c2 - a2 * c1) / det;
            return IsPointOnLine(line1, x, y) && IsPointOnLine(line2, x, y);
        }

        public static Tuple<double, double, double> GetEquation(this LineSegment2D line)
        {
            double a = line.P2.Y - line.P1.Y;
            double b = line.P1.X - line.P2.X;
            double c = a * line.P1.X + b * line.P1.Y;
            return Tuple.Create(a, b, c);
        }

        public static bool IsPointOnLine(LineSegment2D line, double x, double y)
        {
            return Math.Min(line.P1.X, line.P2.X) <= x && Math.Max(line.P1.X, line.P2.X) >= x &&
                   Math.Min(line.P1.Y, line.P2.Y) <= y && Math.Max(line.P1.Y, line.P2.Y) >= y;
        }

        public static List<List<T>> Rotate90<T>(this List<List<T>> markerData)
        {
            return markerData.Select((t1, y) => markerData.Select(t => t[markerData.Count - 1 - y]).ToList()).ToList();
        }

        public static Point AveragePoint(this List<Point> points)
        {
            return new Point((int) points.Select(p => p.X).Average(), (int) points.Select(p => p.Y).Average());
        }
    }
}