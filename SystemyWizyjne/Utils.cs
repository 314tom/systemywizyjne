﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Xml;
using Emgu.CV;

namespace SystemyWizyjne
{
    public static class Utils
    {
        private static readonly Random Rng = new Random();

        public static string ToString1<T>(List<T> data, string separator = ", ")
        {
            return string.Join(separator, data);
        }

        public static string ToString2<T>(List<List<T>> data, string separator1 = ", ", string separator2 = "\n")
        {
            return string.Join(separator2, data.Select(d => ToString1(d, separator1)));
        }

        public static string ToString3<T>(List<List<List<T>>> data, string separator1 = ", ", string separator2 = "\n",
            string separator3 = "\n\n")
        {
            return string.Join(separator3, data.Select(d => ToString2(d, separator1, separator2)));
        }

        public static void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static string GetTimestamp()
        {
            return DateTime.Now.ToString("[yyyy.MM.dd HH-mm-ss]");
        }

        public static List<List<T>> GetKCombs<T>(List<T> list, int length) where T : IComparable
        {
            if (length == 1) return list.Select(t => new List<T> {t}).ToList();
            return GetKCombs(list, length - 1).SelectMany(t => list.Where(o => o.CompareTo(t.Last()) > 0),
                (t1, t2) => t1.Concat(new[] {t2}).ToList()).ToList();
        }


        public static string ToString<T>(this List<T> list)
        {
            return Utils.ToString1(list);
        }

        public static int MaxIndex<T>(this IEnumerable<T> sequence)
            where T : IComparable<T>
        {
            int maxIndex = -1;
            T maxValue = default(T);

            int index = 0;
            foreach (T value in sequence)
            {
                if (value.CompareTo(maxValue) > 0 || maxIndex == -1)
                {
                    maxIndex = index;
                    maxValue = value;
                }
                index++;
            }
            return maxIndex;
        }


        public static BitmapImage BitmapToImageSource(Image bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static Color RandomColor()
        {
            byte[] colors = new byte[3];
            Rng.NextBytes(colors);
            return Color.FromArgb(127, colors[0], colors[1], colors[2]);
        }

        public static Matrix3D RotationXMatrix(double angle)
        {
            Matrix3D matrix3D = Matrix3D.Identity;
            var sin = Math.Sin(angle);
            var cos = Math.Cos(angle);
            matrix3D.M22 = cos;
            matrix3D.M23 = -sin;
            matrix3D.M32 = sin;
            matrix3D.M33 = cos;
            return matrix3D;
        }

        public static Matrix3D RotationYMatrix(double angle)
        {
            Matrix3D matrix3D = Matrix3D.Identity;
            var sin = Math.Sin(angle);
            var cos = Math.Cos(angle);
            matrix3D.M11 = cos;
            matrix3D.M31 = sin;
            matrix3D.M13 = -sin;
            matrix3D.M33 = cos;
            return matrix3D;
        }

        public static Matrix3D RotationZMatrix(double angle)
        {
            Matrix3D matrix3D = Matrix3D.Identity;
            var sin = Math.Sin(angle);
            var cos = Math.Cos(angle);
            matrix3D.M11 = cos;
            matrix3D.M12 = -sin;
            matrix3D.M21 = sin;
            matrix3D.M22 = cos;
            return matrix3D;
        }

        public static Matrix3D ScaleMatrix(double scaleX, double scaleY, double scaleZ)
        {
            Matrix3D matrix3D = Matrix3D.Identity;
            matrix3D.M11 = scaleX;
            matrix3D.M22 = scaleY;
            matrix3D.M33 = scaleZ;
            return matrix3D;
        }

        public static Matrix3D ScaleMatrix(double scale)
        {
            return ScaleMatrix(scale, scale, scale);
        }

        public static Matrix3D TranslateMatrix(double tX, double tY, double tZ)
        {
            Matrix3D matrix3D = Matrix3D.Identity;
            matrix3D.OffsetX = tX;
            matrix3D.OffsetY = tY;
            matrix3D.OffsetZ = tZ;
            return matrix3D;
        }

        public static Matrix3D GetTransformMatrix(RotationVector3D rodrigues, double[] translationVector)
        {
            return new Matrix3D
            (
                rodrigues.RotationMatrix.GetDoubleValue(0, 0), //m11
                rodrigues.RotationMatrix.GetDoubleValue(1, 0), //m12
                rodrigues.RotationMatrix.GetDoubleValue(2, 0), //m13
                0,
                //rodrigues.RotationMatrix.GetDoubleValue(3,0),//m14
                rodrigues.RotationMatrix.GetDoubleValue(0, 1), //m21
                rodrigues.RotationMatrix.GetDoubleValue(1, 1), //m22
                rodrigues.RotationMatrix.GetDoubleValue(2, 1), //m23
                0,
                //rodrigues.RotationMatrix.GetDoubleValue(3,1),//m24
                rodrigues.RotationMatrix.GetDoubleValue(0, 2), //m31
                rodrigues.RotationMatrix.GetDoubleValue(1, 2), //m32
                rodrigues.RotationMatrix.GetDoubleValue(2, 2), //m33
                0,
                //rodrigues.RotationMatrix.GetDoubleValue(3,2),//m34
                translationVector[0], //oX
                translationVector[1], //oY
                translationVector[2], //oZ
                1
                //rodrigues.RotationMatrix.GetDoubleValue(3,3)//m44
            );
        }

        public static T Clone<T>(T source)
        {
            string objXaml = XamlWriter.Save(source);
            StringReader stringReader = new StringReader(objXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            T t = (T) XamlReader.Load(xmlReader);
            return t;
        }
    }
}