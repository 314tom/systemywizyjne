﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Media.Media3D;
using SystemyWizyjne.MyMath;
using SystemyWizyjne.Presentation;
using SystemyWizyjne.SerializableData;
using SystemyWizyjne.Transformations;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Microsoft.Win32;
using Color = System.Drawing.Color;
using Point = System.Drawing.Point;
using PointCollection = Emgu.CV.PointCollection;
using Size = System.Drawing.Size;

namespace SystemyWizyjne
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Configurable_Values

        public int MinimalContourPerimeter { get; set; } = 60;
        public int MinimalContourArea { get; set; } = 250;
        public bool QuadrilateralAveraging { get; set; } = !true;

        public int QuadrilateralAveragingCount
        {
            get => _averager1.LastFrames;
            set
            {
                _averager1.LastFrames = value;
                _averager2.LastFrames = value;
            }
        }

        public int QuadrilateralAveragingDiff
        {
            get => (int) _averager1.MaxDiff;
            set
            {
                _averager1.MaxDiff = value;
                _averager2.MaxDiff = value;
            }
        }

        public int SelectedThresholding { get; set; } = 0;
        public bool AdaptiveThresholding => SelectedThresholding == 0 || SelectedThresholding == 2;
        public bool OtsuThresholding => SelectedThresholding == 1 || SelectedThresholding == 2;
        public int AdaptiveThresholdingBlockSize { get; set; } = 31;
        public int MinimalMarkerDistance { get; set; } = 50;
        public int MarkerBounduaryDiff { get; set; } = 25;
        public int MarkerAvgColorThreshold { get; set; } = 80;

        #endregion

        private Mat _cameraMatrix;
        private Mat _distCoeffs;

        private PoseEstimator _poseEstimator;

        private readonly VideoCapture _capture;
        private readonly List<Color> _colors = new List<Color>();
        private bool _runProcessing = true;

        private readonly QuadrilateralAverager _averager1;
        private readonly QuadrilateralAverager _averager2;

        public ObservableCollection<MappedObject3D> Models { get; }

        public MainWindow()
        {
            Models = new ObservableCollection<MappedObject3D>();
            _averager1 = new QuadrilateralAverager(2, 100);
            _averager2 = new QuadrilateralAverager(2, 100);
            GetInitialModels(@"Data\markers.xml");

            InitializeComponent();
            _poseEstimator = new PoseEstimator();

            for (int i = 0; i < 1000; i++)
            {
                _colors.Add(Utils.RandomColor());
            }

            CvInvoke.UseOpenCL = false;
            try
            {
                _capture = new VideoCapture();
                _capture.ImageGrabbed += ProcessFrameSecure;
                _capture.Start();
                _capture.FlipHorizontal = true;
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
//            Draw3D();
        }

        private void GetInitialModels(string filename)
        {
            try
            {
                var objects = SerializableModels.Read(filename).GetMappedObjects3D();
                if (objects != null && objects.Count > 0)
                {
                    Models.Clear();
                    objects.ForEach(Models.Add);
                }
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private void ProcessFrameSecure(object sender, EventArgs e)
        {
            try
            {
                if (_runProcessing)
                {
                    ProcessFrame(sender, e);
                }
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero)
            {
                Mat frame = new Mat();
                Mat grayFrame = new Mat();
                Mat smallGrayFrame = new Mat();
                Mat smoothedGrayFrame = new Mat();
                Mat thresholdedFrame = new Mat();
                _capture.Retrieve(frame);
                CvInvoke.CvtColor(frame, grayFrame, ColorConversion.Bgr2Gray);
                CvInvoke.PyrDown(grayFrame, smallGrayFrame);
                CvInvoke.PyrUp(smallGrayFrame, smoothedGrayFrame);

                var rawFrame = frame.Clone();
                var detectedMarkers = new List<(ModelVisual3D model, Matrix3D matrix, Point center)>();

                if (AdaptiveThresholding)
                {
                    CvInvoke.AdaptiveThreshold(smoothedGrayFrame, thresholdedFrame, 255, AdaptiveThresholdType.MeanC,
                        ThresholdType.Binary, AdaptiveThresholdingBlockSize, 0);
                    VectorOfVectorOfPoint contours = FindContours(thresholdedFrame);
                    detectedMarkers.AddRange(ProcessQuadrilaterals(rawFrame, thresholdedFrame, contours, _averager1));
                    EmguImage1.Image = thresholdedFrame;
                }
                if (OtsuThresholding)
                {
                    CvInvoke.Threshold(smoothedGrayFrame, thresholdedFrame, 0, 255, ThresholdType.Otsu);
                    VectorOfVectorOfPoint contours = FindContours(thresholdedFrame);
                    detectedMarkers.AddRange(ProcessQuadrilaterals(rawFrame, thresholdedFrame, contours, _averager2));
                    if (AdaptiveThresholding)
                    {
                        EmguImage4.Image = thresholdedFrame;
                    }
                    else
                    {
                        EmguImage1.Image = thresholdedFrame;
                    }
                }

                Image.Dispatcher.Invoke(() => { ImageBackground.Source = Utils.BitmapToImageSource(frame.Bitmap); });
                Draw3DDispatched(RemoveRepeated(detectedMarkers));
                EmguImage3.Image = rawFrame;
            }
        }

        private List<(ModelVisual3D model, Matrix3D matrix)> RemoveRepeated(
            List<(ModelVisual3D model, Matrix3D matrix, Point center)> detectedMarkers)
        {
            var output = new List<(ModelVisual3D model, Matrix3D matrix)>();
            for (var i = 0; i < detectedMarkers.Count; i++)
            {
                bool notDuplicated = true;
                for (var j = i + 1; j < detectedMarkers.Count && notDuplicated; j++)
                {
                    notDuplicated = detectedMarkers[i].center.Distance(detectedMarkers[j].center) >=
                                    MinimalMarkerDistance;
                }
                if (notDuplicated)
                {
                    output.Add((detectedMarkers[i].model, detectedMarkers[i].matrix));
                }
            }
            return output;
        }

        private VectorOfVectorOfPoint FindContours(Mat thresholdedFrame)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            byte[] thresValues = new byte[thresholdedFrame.Height * thresholdedFrame.Width];
            thresholdedFrame.CopyTo(thresValues);
            for (int i = 0; i < thresValues.Length; i++)
            {
                thresValues[i] = (byte) (thresValues[i] == 0 ? 255 : 0);
            }

            Mat reverseThresFrame = new Mat(thresholdedFrame.Size, thresholdedFrame.Depth,
                thresholdedFrame.NumberOfChannels);
            reverseThresFrame.SetTo(thresValues);
            Mat labels = new Mat();
            CvInvoke.ConnectedComponents(reverseThresFrame, labels, LineType.EightConnected);
            int[] labelsPixels = new int[labels.Height * labels.Width];
            labels.CopyTo(labelsPixels);
            List<int> labelsChecked = new List<int>();
            int columns = labels.Cols;
            for (int row = 0; row < labels.Rows; row++)
            {
                for (int col = 0; col < columns; col++)
                {
                    int label = labelsPixels[row * columns + col];
                    if (!labelsChecked.Contains(label))
                    {
                        labelsChecked.Add(label);
                        VectorOfPoint contour = FindContour(label, row, col, columns, labels.Rows, labelsPixels);
                        if (contour.Size > MinimalContourPerimeter)
                            contours.Push(contour);
                    }
                }
            }
            return contours;
        }

        //znajduje kontur figury metodą opisaną w thesis_v3 pod contour detection
        private VectorOfPoint FindContour(int label, int row, int col, int columns, int rows, int[] labelsPixels)
        {
            List<Point> points = new List<Point>();
            int currentRow = row, currentCol = col;
            bool end = false;
            int nextStart = 0;
            while (!end)
            {
                int counterFlag = 0;
                points.Add(new Point(currentCol, currentRow));

                while (counterFlag < 8)
                {
                    switch (nextStart)
                    {
                        case 0:
                            if (currentRow > 0 &&
                                labelsPixels[(currentRow - 1) * columns + currentCol] == label)
                            {
                                currentRow--;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 1:
                            if (currentRow > 0 && currentCol < (columns - 1) &&
                                labelsPixels[(currentRow - 1) * columns + currentCol + 1] == label)
                            {
                                currentRow--;
                                currentCol++;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 2:
                            if (currentCol < (columns - 1) &&
                                labelsPixels[(currentRow) * columns + currentCol + 1] == label)
                            {
                                currentCol++;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 3:
                            if ((currentRow < rows - 1) && currentCol < (columns - 1) &&
                                labelsPixels[(currentRow + 1) * columns + currentCol + 1] == label)
                            {
                                currentRow++;
                                currentCol++;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 4:
                            if ((currentRow < rows - 1) &&
                                labelsPixels[(currentRow + 1) * columns + currentCol] == label)
                            {
                                currentRow++;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 5:
                            if ((currentRow < rows - 1) && currentCol > 0 &&
                                labelsPixels[(currentRow + 1) * columns + currentCol - 1] == label)
                            {
                                currentRow++;
                                currentCol--;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 6:
                            if (currentCol > 0 &&
                                labelsPixels[(currentRow) * columns + currentCol - 1] == label)
                            {
                                currentCol--;
                                counterFlag = 8;
                            }
                            nextStart++;
                            break;
                        case 7:
                            if ((currentRow > 0) && currentCol > 0 &&
                                labelsPixels[(currentRow - 1) * columns + currentCol - 1] == label)
                            {
                                currentRow--;
                                currentCol--;
                                counterFlag = 8;
                            }
                            nextStart = 0;
                            break;
                    }
                    counterFlag++;
                }

                nextStart = (nextStart + 4) % 8;
                end = currentRow == row && currentCol == col;
            }
            return new VectorOfPoint(points.ToArray());
        }

        #region ProcessConnectedComponentLabeling - wykrywanie wierzchołków i czworokątów, zrezygnowano

        private void ProcessConnectedComponentLabeling(Mat image, Mat frame)
        {
            try
            {
                Mat labels = new Mat();
                CvInvoke.ConnectedComponents(image, labels, LineType.EightConnected);
                int[] labelsPixels = new int[labels.Height * labels.Width];
                labels.CopyTo(labelsPixels);
                int columns = labels.Cols;
                int maxLabel = labelsPixels.Max();
                int[] numberOfVertexesDetected = new int[maxLabel + 1];
                for (int i = 0; i < maxLabel; i++)
                {
                    numberOfVertexesDetected[i] = 0;
                }

                List<(int label, int x, int y)> resultList = new List<(int label, int x, int y)>();

                int radius = 5; //NIEPARZYSTE!
                int thresEdge = ((2 * radius) + 2 * (radius - 2) - 2) / 2;
                for (int row = 0; row < labels.Rows; row++)
                {
                    for (int col = 0; col < columns; col++)
                    {
                        if (row <= (radius / 2) - 1 || row >= labels.Rows - (radius / 2) || col <= (radius / 2) - 1 ||
                            col >= columns - (radius / 2)) //TEMP
                            continue;

                        int currentLabel = labelsPixels[row * columns + col];
                        int numberOfOtherLabelPixels = 0;
                        int numberOfSameLabelPixels = 0;

                        for (int i = -(radius / 2); i < (radius / 2) + 1; i++)
                        {
                            if (labelsPixels[(row - (radius / 2)) * columns + (col + i)] == currentLabel)
                                numberOfSameLabelPixels++;
                            else
                                numberOfOtherLabelPixels++;

                            if (labelsPixels[(row + (radius / 2)) * columns + (col + i)] == currentLabel)
                                numberOfSameLabelPixels++;
                            else
                                numberOfOtherLabelPixels++;
                        }

                        for (int i = -((radius / 2) - 1); i < (radius / 2); i++)
                        {
                            if (labelsPixels[(row + i) * columns + (col - (radius / 2))] == currentLabel)
                                numberOfSameLabelPixels++;
                            else
                                numberOfOtherLabelPixels++;
                            if (labelsPixels[(row + i) * columns + (col + (radius / 2))] == currentLabel)
                                numberOfSameLabelPixels++;
                            else
                                numberOfOtherLabelPixels++;
                        }

                        #region TEMP

                        //if (row == 1 || row == labels.Rows - 2)
                        //{
                        //    numberOfOtherLabelPixels += 5;
                        //}

                        //if (row == 0 || row == labels.Rows - 1)
                        //{
                        //    numberOfOtherLabelPixels += 7;
                        //}

                        //if (col == 1 || col == labels.Rows - 2)
                        //    numberOfOtherLabelPixels += 5;
                        //if (col == 0 || col == labels.Rows - 1)
                        //    numberOfOtherLabelPixels += 2;

                        #endregion

                        bool isCorner = numberOfSameLabelPixels <= thresEdge;

                        if (isCorner)
                        {
                            numberOfVertexesDetected[currentLabel]++;
                            resultList.Add((currentLabel, row, col));
                        }
                    }
                }
                for (int i = 0; i < numberOfVertexesDetected.Length; i++)
                {
                    foreach ((int label, int x, int y) tuple in resultList)
                    {
                        if (tuple.label == i)
                        {
                            CvInvoke.Circle(frame, new Point(tuple.y, tuple.x), 5, new Bgr(_colors[i]).MCvScalar, 1);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        #endregion ProcessConnectedComponentLabeling

        private List<(ModelVisual3D model, Matrix3D matrix, Point center)> ProcessQuadrilaterals(Mat rawFrame,
            Mat thresholdedFrame, VectorOfVectorOfPoint contours, QuadrilateralAverager averager)
        {
            List<MyQuadrilateral> quads = GetQuadrilaterals(contours);

            //TODO remove
            foreach (LineSegment2D line in quads.SelectMany(q => q.GetLines()))
            {
                CvInvoke.Line(rawFrame, line.P1, line.P2, new Bgr(Color.GreenYellow).MCvScalar, 2);
            }
            //TODO remove

            if (QuadrilateralAveraging)
            {
                var avgquads = averager.Process(quads);
                //TODO remove
                foreach (var line in avgquads.SelectMany(q => q.GetLines()))
                {
                    CvInvoke.Line(rawFrame, line.P1, line.P2, new Bgr(Color.Blue).MCvScalar, 1);
                }
                //TODO remove
                quads = avgquads;
            }
            return DetectMarkers(rawFrame, thresholdedFrame, quads);
        }

        private List<(ModelVisual3D model, Matrix3D matrix, Point center)> DetectMarkers(Mat rawFrame,
            Mat thresholdedFrame, List<MyQuadrilateral> quads)
        {
            var drawParams = new List<(ModelVisual3D model, Matrix3D matrix, Point center)>();

            foreach (MyQuadrilateral quad in quads)
            {
                Image<Bgr, Byte> img = thresholdedFrame.ToImage<Bgr, Byte>();
                const int size = 400;
                Image<Bgr, byte> image = img.GetAxisAlignedImagePart(quad, new Size(size, size));
                List<List<bool?>> markerGrid = DrawGrid(image, 5, size * MarkerBounduaryDiff / (100d * 7));
                List<int> markersDetected = MarkersDetected(markerGrid);
                int index = markersDetected.FindIndex(val => val != -1);
                if (index >= 0 && !drawParams.Any(dP => ReferenceEquals(dP.model, Models[index].Model)))
                {
                    int rotation = markersDetected[index];
                    quad.SetRotation(rotation);
                    if (SelectedThresholding != 2)
                    {
                        EmguImage4.Image = image;
                    }

                    //TODO remove
                    foreach (var line in quad.GetLines())
                    {
                        CvInvoke.Line(rawFrame, line.P1, line.P2, new Bgr(Color.Red).MCvScalar, 2);
                    }
                    CvInvoke.Circle(rawFrame, quad.Center(), 5, new Bgr(Color.Red).MCvScalar, 2);
                    LineSegment2D lineSegment2D = new LineSegment2D(quad.GetPoint(rotation), quad.Center());
                    CvInvoke.Circle(rawFrame, quad.GetPoint(rotation), 4, new Bgr(Color.Blue).MCvScalar, 1);
                    CvInvoke.Line(rawFrame, lineSegment2D.P1, lineSegment2D.P2, new Bgr(Color.Blue).MCvScalar);
                    //TODO remove

                    drawParams.Add(
                        (Models[index].Model, _poseEstimator.Estimate(rawFrame, quad, Models[index].MarkerWidth), quad
                            .Center()));
                }
            }
            return drawParams;
        }

        private List<MyQuadrilateral> GetQuadrilaterals(VectorOfVectorOfPoint contours)
        {
            List<MyQuadrilateral> quads = new List<MyQuadrilateral>();
            int count = contours.Size;
            for (int i = 0; i < count; i++)
            {
                using (VectorOfPoint contour = contours[i])
                using (VectorOfPoint approxContour = new VectorOfPoint())
                {
                    CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                    if (CvInvoke.ContourArea(approxContour) > MinimalContourArea && approxContour.Size == 4)
                    {
                        Point[] points = approxContour.ToArray();
                        LineSegment2D[] edges = PointCollection.PolyLine(points, true);
                        if (!(edges[0].IntersectsInside(edges[2]) || edges[1].IntersectsInside(edges[3])))
                        {
                            quads.Add(new MyQuadrilateral(points[0], points[1], points[2], points[3]));
                        }
                    }
                }
            }
            return quads;
        }

        private List<int> MarkersDetected(List<List<bool?>> markerGrid)
        {
            return Models.Select(m => m.MarkerData.Equals(markerGrid)).ToList();
        }

        private List<List<bool?>> DrawGrid(Image<Bgr, byte> image, int size, double diff)
        {
            int width = image.Size.Width;
            int height = image.Size.Height;
            double widthDiff = (double) width / (size + 2);
            double heightDiff = (double) height / (size + 2);
            List<List<bool?>> imgData = new List<List<bool?>>();
            for (int y = 1; y <= size; y++)
            {
                List<bool?> row = new List<bool?>();
                for (int x = 1; x <= size; x++)
                {
                    int posY = (int) (y * heightDiff + diff);
                    int posX = (int) (x * widthDiff + diff);
                    int rectWidth = (int) (widthDiff - 2 * diff);
                    int rectHeight = (int) (heightDiff - 2 * diff);
                    Rectangle rectangle = new Rectangle(posX, posY, rectWidth, rectHeight);
                    int red = (int) image.Copy(rectangle).GetAverage().Red;
                    var val = PixelThresholding(red);
                    row.Add(val);
                    CvInvoke.Rectangle(image, rectangle, new Bgr(GetColor(val)).MCvScalar, 2);
                }
                imgData.Add(row);
            }
            return imgData;
        }

        private bool? PixelThresholding(int avg)
        {
            int min = 255 - MarkerAvgColorThreshold;
            bool? val = null;
            if (avg > min) val = true;
            else if (avg < MarkerAvgColorThreshold) val = false;
            return val;
        }

        private static Color GetColor(bool? val)
        {
            return !val.HasValue ? Color.Yellow : val.Value ? Color.Red : Color.Green;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _capture.Dispose();
        }

        #region Drawing3D

        private void Draw3DDispatched(List<(ModelVisual3D model, Matrix3D transformMatrix)> drawParams)
        {
            Dispatcher.Invoke(() => Draw3D(drawParams));
        }

        private void Draw3D(List<(ModelVisual3D model, Matrix3D transformMatrix)> drawParams)
        {
            var objects = MainViewPort.Children.ToList().Take(1).ToList();
            MainViewPort.Children.Clear();
            objects.ForEach(MainViewPort.Children.Add);
            foreach (var drawParam in drawParams)
            {
                drawParam.model.Transform = new MatrixTransform3D(drawParam.transformMatrix);
                MainViewPort.Children.Add(Utils.Clone(drawParam.model));
            }
        }

        #endregion


        private void DeleteItemClick(object sender, RoutedEventArgs e)
        {
            Models.RemoveAt(RightMenu.SelectedIndex);
        }

        private void AddNewObject(object sender, RoutedEventArgs e)
        {
            var window = new Presentation.MarkerAndModelCreatorWindow(5, 5, Models.Select(m => m.MarkerData).ToList());
            if (window.ShowDialog() != true)
            {
                return;
            }
            Models.Add(window.Model);
        }

        private void OpenFromFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "XML files (.xml)|*.xml"
            };
            if (dialog.ShowDialog() != true) return;
            try
            {
                var objects = SerializableModels.Read(dialog.FileName).GetMappedObjects3D();
                if (objects != null && objects.Count > 0)
                {
                    Models.Clear();
                    objects.ForEach(Models.Add);
                }
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private void SaveToFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                Filter = "XML files (.xml)|*.xml"
            };
            if (dialog.ShowDialog() != true) return;
            try
            {
                new SerializableModels(Models.ToList()).SaveData(dialog.FileName);
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private void CalibrateCamera(object sender, RoutedEventArgs e)
        {
            CameraCalibrationWindow cameraCalibrationWindow = new CameraCalibrationWindow();
            _runProcessing = false;
            bool? calibrated = cameraCalibrationWindow.ShowDialog();
            if (calibrated == true)
            {
                _cameraMatrix = cameraCalibrationWindow.CameraMatrix;
                _distCoeffs = cameraCalibrationWindow.DistCoeffs;
                _poseEstimator = new PoseEstimator(_cameraMatrix, _distCoeffs);
            }
            _runProcessing = true;
        }
    }
}