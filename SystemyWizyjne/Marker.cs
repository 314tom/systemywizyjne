﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;
using Brushes = System.Drawing.Brushes;

namespace SystemyWizyjne
{
    public class Marker
    {
        public ImageSource Image => Utils.BitmapToImageSource(ToBitmap());

        public List<List<bool>> Values => _basicData;
        private readonly List<List<bool>> _basicData;
        private readonly List<List<bool>> _rotated90;
        private readonly List<List<bool>> _rotated180;
        private readonly List<List<bool>> _rotated270;

        public Marker(List<List<bool>> basicData)
        {
            _basicData = basicData;
            _rotated90 = _basicData.Rotate90();
            _rotated180 = _rotated90.Rotate90();
            _rotated270 = _rotated180.Rotate90();
        }

        public int Equals(Marker m)
        {
            return Equals(m.Values);
        }

        public int Equals(List<List<bool?>> markerData2)
        {
            if (_basicData.Count != markerData2?.Count)
                return -1;
            return EqualsList(_basicData, markerData2)
                ? 0
                : EqualsList(_rotated90, markerData2)
                    ? 1
                    : EqualsList(_rotated180, markerData2)
                        ? 2
                        : EqualsList(_rotated270, markerData2)
                            ? 3
                            : -1;
        }

        public int Equals(List<List<bool>> markerData2)
        {
            if (_basicData.Count != markerData2?.Count)
                return -1;
            return EqualsList(_basicData, markerData2)
                ? 0
                : EqualsList(_rotated90, markerData2)
                    ? 1
                    : EqualsList(_rotated180, markerData2)
                        ? 2
                        : EqualsList(_rotated270, markerData2)
                            ? 3
                            : -1;
        }

        public Marker GetYSymmetric()
        {
            return new Marker(Values.Select(row =>
            {
                List<bool> r = new List<bool>(row);
                r.Reverse();
                return r;
            }).ToList());
        }

        public Marker Rotated()
        {
            return new Marker(_rotated90);
        }

        public Marker GetXSymmetric()
        {
            List<List<bool>> v = new List<List<bool>>(Values);
            v.Reverse();
            return new Marker(v);
        }

        private static bool EqualsList(List<List<bool>> thisMarker, List<List<bool?>> other)
        {
            bool areEqual = true;
            for (int i = 0; i < other.Count && areEqual; i++)
            {
                for (int j = 0; j < other.Count && areEqual; j++)
                {
                    areEqual = other[i][j] == thisMarker[i][j];
                }
            }
            return areEqual;
        }

        private static bool EqualsList(List<List<bool>> thisMarker, List<List<bool>> other)
        {
            bool areEqual = true;
            for (int i = 0; i < other.Count && areEqual; i++)
            {
                for (int j = 0; j < other.Count && areEqual; j++)
                {
                    areEqual = other[i][j] == thisMarker[i][j];
                }
            }
            return areEqual;
        }

        public override string ToString()
        {
            return _basicData.Aggregate("",
                (current1, row) => row.Aggregate(current1, (current, b) => current + (b ? "#" : "_")) + "\n");
        }

        private Bitmap ToBitmap()
        {
            const int width = 84;
            const int height = width;
            Bitmap bitmap = new Bitmap(width, height);
            int yCount = _basicData.Count + 2;
            int xCount = _basicData[0].Count + 2;

            double diffX = (double) width / xCount;
            double diffY = (double) height / yCount;
            Marker symmetric = GetYSymmetric();

            using (Graphics graph = Graphics.FromImage(bitmap))
            {
                Rectangle imageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.Black, imageSize);
                for (int y = 0; y < yCount - 2; y++)
                {
                    for (int x = 0; x < xCount - 2; x++)
                    {
                        Rectangle r = new Rectangle((int) (x * diffX + diffX), (int) (y * diffY + diffY), (int) diffX,
                            (int) diffY);
                        graph.FillRectangle(symmetric._rotated270[y][x] ? Brushes.White : Brushes.Black, r);
                    }
                }
            }
            return bitmap;
        }
    }
}