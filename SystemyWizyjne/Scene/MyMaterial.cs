﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SystemyWizyjne.Scene
{
    public class MyMaterial
    {
        public string Name;
        public float Ns;
        public float KaR;
        public float KaG;
        public float KaB;
        public float KdR;
        public float KdG;
        public float KdB;
        public float KsR;
        public float KsG;
        public float KsB;
        public float KeR;
        public float KeG;
        public float KeB;
        public float Ni;
        public float D;
        public int Illum;


        public MyMaterial(string[] lines)
        {
            Name = lines[0].Split(' ')[1];
            Ns = float.Parse(lines[1].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            KaR = float.Parse(lines[2].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            KaG = float.Parse(lines[2].Split(' ')[2], new System.Globalization.CultureInfo("en-US"));
            KaB = float.Parse(lines[2].Split(' ')[3], new System.Globalization.CultureInfo("en-US"));
            KdR = float.Parse(lines[3].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            KdG = float.Parse(lines[3].Split(' ')[2], new System.Globalization.CultureInfo("en-US"));
            KdB = float.Parse(lines[3].Split(' ')[3], new System.Globalization.CultureInfo("en-US"));
            KsR = float.Parse(lines[4].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            KsG = float.Parse(lines[4].Split(' ')[2], new System.Globalization.CultureInfo("en-US"));
            KsB = float.Parse(lines[4].Split(' ')[3], new System.Globalization.CultureInfo("en-US"));
            KeR = float.Parse(lines[5].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            KeG = float.Parse(lines[5].Split(' ')[2], new System.Globalization.CultureInfo("en-US"));
            KeB = float.Parse(lines[5].Split(' ')[3], new System.Globalization.CultureInfo("en-US"));
            Ni = float.Parse(lines[6].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            D = float.Parse(lines[7].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
            Illum = int.Parse(lines[8].Split(' ')[1], new System.Globalization.CultureInfo("en-US"));
        }

        public override string ToString()
        {
            string s = "";
            s += "Name: " + Name + "\n";
            s += "Ns: " + Ns + "\n";
            s += "KaR: " + KaR + " KaG: " + KaG + " KaB: " + KaB + "\n";
            s += "KdR: " + KdR + " KdG: " + KdG + " KdB: " + KdB + "\n";
            s += "KsR: " + KsR + " KsG: " + KsG + " KsB: " + KsB + "\n";
            s += "KeR: " + KeR + " KeG: " + KeG + " KeB: " + KaB + "\n";
            s += "Ni: " + Ni + "\n";
            s += "d: " + D + "\n";
            s += "illum: " + Illum;
            return s;
        }

        public DiffuseMaterial GetDiffuseMaterial()
        {
            return new DiffuseMaterial(new SolidColorBrush(GetColor(KdR, KdG, KdB)));
        }

        public SpecularMaterial GetSpecularMaterial()
        {
            return new SpecularMaterial(new SolidColorBrush(GetColor(KdR, KdG, KdB)), Ns);
        }

        private Color GetColor(float r, float g, float b)
        {
            byte bR = (byte) (r * 255);
            byte bG = (byte) (g * 255);
            byte bB = (byte) (b * 255);
            return Color.FromRgb(bR, bG, bB);
        }
    }
}