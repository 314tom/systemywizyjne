﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SystemyWizyjne.Scene
{
    public class SceneReader
    {
        private const double Scale = 0.05;

        public static ModelVisual3D Model()
        {
            Model3DGroup triangle = new Model3DGroup();
            var val2 = -1;
            var val1 = 1;
            Point3D p0 = new Point3D(val2, val2, val2);
            Point3D p1 = new Point3D(val1, val2, val2);
            Point3D p2 = new Point3D(val1, val2, val1);
            Point3D p3 = new Point3D(val2, val2, val1);
            Point3D p4 = new Point3D(val2, val1, val2);
            Point3D p5 = new Point3D(val1, val1, val2);
            Point3D p6 = new Point3D(val1, val1, val1);

            triangle.Children.Add(CreateTriangleModel(p1, p3, p4));
            triangle.Children.Add(CreateTriangleModel(p1, p4, p6));
            triangle.Children.Add(CreateTriangleModel(p3, p1, p6));
            triangle.Children.Add(CreateTriangleModel(p4, p3, p6));
            return new ModelVisual3D {Content = triangle};
        }


        private static Model3D CreateTriangleModel(Point3D p0, Point3D p1, Point3D p2)
        {
            MeshGeometry3D mymesh = new MeshGeometry3D();
            mymesh.Positions.Add(p0);
            mymesh.Positions.Add(p1);
            mymesh.Positions.Add(p2);
            mymesh.TriangleIndices.Add(0);
            mymesh.TriangleIndices.Add(1);
            mymesh.TriangleIndices.Add(2);
            Vector3D normal = CalculateTraingleNormal(p0, p1, p2);
            mymesh.Normals.Add(normal);
            mymesh.Normals.Add(normal);
            mymesh.Normals.Add(normal);
            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.BlueViolet));
            GeometryModel3D model = new GeometryModel3D(mymesh, material);
            Model3DGroup group = new Model3DGroup();
            group.Children.Add(model);
            return group;
        }

        private static Vector3D CalculateTraingleNormal(Point3D p0, Point3D p1, Point3D p2)
        {
            Vector3D v0 = new Vector3D(p1.X - p0.X, p1.Y - p0.Y, p1.Z - p0.Z);
            Vector3D v1 = new Vector3D(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
            return Vector3D.CrossProduct(v0, v1);
        }

        public static ModelVisual3D ReadScene(string filename)
        {
            string path = filename.Substring(0, filename.LastIndexOf('\\') < 0 ? 0 : filename.LastIndexOf('\\'));
            StreamReader sr = new StreamReader(filename);
            List<Point3D> points = new List<Point3D>();
            List<Vector3D> normals = new List<Vector3D>();
            List<MyMaterial> materials = new List<MyMaterial>();
            MyMaterial lastMaterial = null;


            Dictionary<MyMaterial, List<Triangle3D>> objects = new Dictionary<MyMaterial, List<Triangle3D>>();
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line == null)
                    continue;
                string identifier = line.Split(' ')[0];
                if (identifier == "mtllib")
                    materials = ReadMaterials(path + "\\" + line.Split(' ')[1]);
                if (identifier == "v")
                {
                    double x = Double.Parse(line.Split(' ')[1], new CultureInfo("en-US")) * Scale;
                    double y = Double.Parse(line.Split(' ')[2], new CultureInfo("en-US")) * Scale;
                    double z = Double.Parse(line.Split(' ')[3], new CultureInfo("en-US")) * Scale;
                    points.Add(new Point3D(x, y, z));
                }
                if (identifier == "vn")
                {
                    double x = Double.Parse(line.Split(' ')[1], new CultureInfo("en-US")) * Scale;
                    double y = Double.Parse(line.Split(' ')[2], new CultureInfo("en-US")) * Scale;
                    double z = Double.Parse(line.Split(' ')[3], new CultureInfo("en-US")) * Scale;
                    normals.Add(new Vector3D(x, y, z));
                }
                if (identifier == "usemtl")
                {
                    lastMaterial = IndexOfMaterial(materials, line.Split(' ')[1]);
                    if (!objects.ContainsKey(lastMaterial))
                    {
                        objects.Add(lastMaterial, new List<Triangle3D>());
                    }
                }
                if (identifier == "f")
                {
                    Point3D point1 = points[Int32.Parse(line.Split(' ', '/')[1]) - 1];
                    Point3D point2 = points[Int32.Parse(line.Split(' ', '/')[4]) - 1];
                    Point3D point3 = points[Int32.Parse(line.Split(' ', '/')[7]) - 1];
                    Vector3D normal1 = normals[Int32.Parse(line.Split(' ', '/')[3]) - 1];
                    Vector3D normal2 = normals[Int32.Parse(line.Split(' ', '/')[6]) - 1];
                    Vector3D normal3 = normals[Int32.Parse(line.Split(' ', '/')[9]) - 1];
                    if (lastMaterial != null)
                    {
                        objects[lastMaterial].Add(new Triangle3D(point1, point2, point3, normal1, normal2, normal3));
                    }
                }
            }
            return new ModelVisual3D {Content = CreateScene(objects)};
        }

        private static Model3DGroup CreateScene(Dictionary<MyMaterial, List<Triangle3D>> objects)
        {
            Model3DGroup model3DGroup = new Model3DGroup();
            foreach (var keyValuePair in objects)
            {
                model3DGroup.Children.Add(CreateObject(keyValuePair.Key, keyValuePair.Value));
            }
            return model3DGroup;
        }

        private static GeometryModel3D CreateObject(MyMaterial myMaterial, List<Triangle3D> triangles)
        {
            MeshGeometry3D mymesh = new MeshGeometry3D();
            List<Point3D> points = triangles
                .SelectMany(triangle => new List<Point3D> {triangle.V1, triangle.V2, triangle.V3}).ToList();
            List<Vector3D> normals = triangles
                .SelectMany(triangle => new List<Vector3D> {triangle.Vn1, triangle.Vn2, triangle.Vn3})
                .ToList();
            points.ForEach(mymesh.Positions.Add);
            for (int i = 0; i < points.Count; i++)
            {
                mymesh.TriangleIndices.Add(i);
            }
            normals.ForEach(mymesh.Normals.Add);
            MaterialGroup mg = new MaterialGroup
            {
                Children = new MaterialCollection(new List<Material>
                {
                    myMaterial.GetDiffuseMaterial(),
                    myMaterial.GetSpecularMaterial()
                })
            };
            return new GeometryModel3D(mymesh, mg);
        }

        public static List<MyMaterial> ReadMaterials(string filename)
        {
            StreamReader sr = new StreamReader(filename);
            sr.ReadLine();
            sr.ReadLine();
            sr.ReadLine();
            List<MyMaterial> materials = new List<MyMaterial>();
            while (!sr.EndOfStream)
            {
                string[] lines = new string[9];
                for (int i = 0; i < 9; i++)
                    lines[i] = sr.ReadLine();
                materials.Add(new MyMaterial(lines));
                sr.ReadLine();
            }
            return materials;
        }

        private static MyMaterial IndexOfMaterial(List<MyMaterial> materials, string name)
        {
            return materials.Find(material => material.Name == name);
        }


        private class Triangle3D
        {
            public Point3D V1 { get; }
            public Point3D V2 { get; }
            public Point3D V3 { get; }
            public Vector3D Vn1 { get; }
            public Vector3D Vn2 { get; }
            public Vector3D Vn3 { get; }

            public Triangle3D(Point3D v1, Point3D v2, Point3D v3, Vector3D vn1, Vector3D vn2, Vector3D vn3)
            {
                V1 = v1;
                V2 = v2;
                V3 = v3;
                Vn1 = vn1;
                Vn2 = vn2;
                Vn3 = vn3;
            }
        }
    }
}