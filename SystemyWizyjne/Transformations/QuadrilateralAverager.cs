﻿using System.Collections.Generic;
using System.Linq;
using SystemyWizyjne.MyMath;

namespace SystemyWizyjne.Transformations
{
    public class QuadrilateralAverager
    {
        public int LastFrames { get; set; }
        public double MaxDiff { get; set; }

        private List<List<MyQuadrilateral>> _oldFrames;
        private List<MyQuadrilateral> _averageQuadrilaterals;

        public QuadrilateralAverager(int lastFrames, int maxDiff)
        {
            LastFrames = lastFrames;
            MaxDiff = maxDiff;
            _oldFrames = new List<List<MyQuadrilateral>>();
            _averageQuadrilaterals = new List<MyQuadrilateral>();
        }

        public List<MyQuadrilateral> Process(List<MyQuadrilateral> currentFrame)
        {
            _averageQuadrilaterals = Average(_oldFrames);
            var matching = MatchingInFrames(_averageQuadrilaterals, currentFrame);
            _oldFrames.Insert(0, currentFrame);
            _oldFrames = _oldFrames.Take(LastFrames).ToList();

            List<MyQuadrilateral> found = new List<MyQuadrilateral>();
            found.AddRange(matching.matching);
            found.AddRange(matching.removed);
            return found;
        }

        private (List<MyQuadrilateral> matching, List<MyQuadrilateral> added, List<MyQuadrilateral> removed)
            MatchingInFrames(List<MyQuadrilateral> oldFrame, List<MyQuadrilateral> newFrame)
        {
            List<(MyQuadrilateral oldQ, MyQuadrilateral newQ)> matchingPairs =
                new List<(MyQuadrilateral, MyQuadrilateral)>();
            List<MyQuadrilateral> notFoundInNew = new List<MyQuadrilateral>();
            List<int> notFoundInOldIndexes = Enumerable.Range(0, newFrame.Count).ToList();
            foreach (MyQuadrilateral q1 in oldFrame)
            {
                bool found = false;
                for (var i = 0; i < newFrame.Count; i++)
                {
                    var q2 = newFrame[i];
                    if (Match(q1, q2))
                    {
                        notFoundInOldIndexes.Remove(i);
                        matchingPairs.Add((q1, q2));
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    notFoundInNew.Add(q1);
                }
            }
            var notFoundInOld = notFoundInOldIndexes.Select(i => newFrame[i]).ToList();
            var matching = matchingPairs
                .Select(m => MyQuadrilateral.Average(new List<MyQuadrilateral>() {m.oldQ, m.newQ})).ToList();
            return (matching, notFoundInOld, notFoundInNew);
        }

        public bool Match(MyQuadrilateral q1, MyQuadrilateral q2)
        {
            return q1.Points.Zip(q2.Points, (p1, p2) => (f: p1, s: p2)).Select(p12 => p12.f.Distance(p12.s)).Sum() <
                   MaxDiff;
        }

        public List<MyQuadrilateral> Average(List<List<MyQuadrilateral>> frames)
        {
            if (frames.Count == 0) return new List<MyQuadrilateral>();
            List<List<MyQuadrilateral>> matching = frames[0].Select(q => new List<MyQuadrilateral>() {q}).ToList();
            for (var i = 1; i < frames.Count; i++)
            {
                foreach (var quad in frames[i])
                {
                    foreach (List<MyQuadrilateral> matched in matching.Where(
                        matched => matched.TrueForAll(q1 => Match(q1, quad))))
                    {
                        matched.Add(quad);
                        break;
                    }
                }
                matching = matching.Where(m => m.Count == i + 1).ToList();
            }
            return matching.Where(m => m.Count > 0).Select(MyQuadrilateral.Average).ToList();
        }
    }
}