﻿using System.Drawing;
using System.Windows.Media.Media3D;
using SystemyWizyjne.MyMath;
using Emgu.CV;
using Emgu.CV.Aruco;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;

namespace SystemyWizyjne.Transformations
{
    public class PoseEstimator
    {
        private readonly Mat _cameraMatrix;
        private readonly Mat _distCoefficients;

        public PoseEstimator(Mat cameraMatrix, Mat distCoefficients)
        {
            _cameraMatrix = cameraMatrix;
            _distCoefficients = distCoefficients;
        }

        public PoseEstimator()
        {
//            _cameraMatrix = new Mat(3, 3, DepthType.Cv64F, 1);
//            _cameraMatrix.SetDoubleValue(0, 0, 160);
//            _cameraMatrix.SetDoubleValue(0, 1, 0);
//            _cameraMatrix.SetDoubleValue(0, 2, 320);
//            _cameraMatrix.SetDoubleValue(1, 0, 0);
//            _cameraMatrix.SetDoubleValue(1, 1, 160);
//            _cameraMatrix.SetDoubleValue(1, 2, 240);
//            _cameraMatrix.SetDoubleValue(2, 0, 0);
//            _cameraMatrix.SetDoubleValue(2, 1, 0);
//            _cameraMatrix.SetDoubleValue(2, 2, 1);
//
//            _distCoefficients = new Mat(8, 1, DepthType.Cv64F, 1);
//            _distCoefficients.SetDoubleValue(0, 0, -0.00162538743992105);
//            _distCoefficients.SetDoubleValue(1, 0, 0.000513511398580673);
//            _distCoefficients.SetDoubleValue(2, 0, 0.000345312408128062);
//            _distCoefficients.SetDoubleValue(3, 0, 0.000790759007007139);
//            _distCoefficients.SetDoubleValue(4, 0, -0.000125784807567078);
//            _distCoefficients.SetDoubleValue(5, 0, 0.00162538743992378);
//            _distCoefficients.SetDoubleValue(6, 0, -0.00051351139858042);
//            _distCoefficients.SetDoubleValue(7, 0, 0.000125784807568042);


            _cameraMatrix = new Mat(3, 3, DepthType.Cv64F, 1);
            _cameraMatrix.SetDoubleValue(0, 0, 464.037690138521);
            _cameraMatrix.SetDoubleValue(0, 1, 0);
            _cameraMatrix.SetDoubleValue(0, 2, 320.018424117005);
            _cameraMatrix.SetDoubleValue(1, 0, 0);
            _cameraMatrix.SetDoubleValue(1, 1, 458.06669096271);
            _cameraMatrix.SetDoubleValue(1, 2, 240.016768668872);
            _cameraMatrix.SetDoubleValue(2, 0, 0);
            _cameraMatrix.SetDoubleValue(2, 1, 0);
            _cameraMatrix.SetDoubleValue(2, 2, 1);

            _distCoefficients = new Mat(14, 1, DepthType.Cv64F, 1);
            _distCoefficients.SetDoubleValue(0, 0, -0.00724863935340576);
            _distCoefficients.SetDoubleValue(1, 0, -0.0182981651446971);
            _distCoefficients.SetDoubleValue(2, 0, 0.00332998855593884);
            _distCoefficients.SetDoubleValue(3, 0, -0.000534537419536433);
            _distCoefficients.SetDoubleValue(4, 0, 0.0171014710585486);
            _distCoefficients.SetDoubleValue(5, 0, 0.00724863935340038);
            _distCoefficients.SetDoubleValue(6, 0, 0.01829816514468);
            _distCoefficients.SetDoubleValue(7, 0, -0.0171014710585398);
            _distCoefficients.SetDoubleValue(8, 0, 0);
            _distCoefficients.SetDoubleValue(9, 0, 0);
            _distCoefficients.SetDoubleValue(10, 0, 0);
            _distCoefficients.SetDoubleValue(11, 0, 0);
            _distCoefficients.SetDoubleValue(12, 0, 0);
            _distCoefficients.SetDoubleValue(13, 0, 0);
        }

        public Matrix3D Estimate(Mat rawFrame, MyQuadrilateral quadrilateral, float markerWidth)
        {
            VectorOfVectorOfPointF corners = new VectorOfVectorOfPointF();
            PointF[] points = quadrilateral.GetPointsF();
            VectorOfPointF corn = new VectorOfPointF();
            corn.Push(points);
            corners.Push(corn);

            Mat rvecs = new Mat();
            Mat tvecs = new Mat();
            ArucoInvoke.EstimatePoseSingleMarkers(corners, markerWidth, _cameraMatrix, _distCoefficients, rvecs,
                tvecs);

            using (Mat rvecMat = rvecs.Row(0))
            using (Mat tvecMat = tvecs.Row(0))
            using (VectorOfDouble rvec = new VectorOfDouble())
            using (VectorOfDouble tvec = new VectorOfDouble())
            {
                double[] values = new double[3];
                rvecMat.CopyTo(values);
                rvec.Push(values);
                RotationVector3D rotationMat = new RotationVector3D(values);
                tvecMat.CopyTo(values);
                tvec.Push(values);
                ArucoInvoke.DrawAxis(rawFrame, _cameraMatrix, _distCoefficients, rvec, tvec,
                    markerWidth * 0.5f); //TODO remove
                return Utils.GetTransformMatrix(rotationMat, values);
            }
        }
    }
}