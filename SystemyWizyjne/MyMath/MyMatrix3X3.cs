﻿using System;
using System.Linq;
using System.Windows.Media.Media3D;
using Emgu.CV;

namespace SystemyWizyjne.MyMath
{
    public class MyMatrix3X3
    {
        private double[] _matrix;
        private MyMatrix3X3 _invertedMatrix;

        public MyMatrix3X3(double[] m)
        {
            _matrix = m;
        }

        public MyMatrix3X3() : this(new double[] {1, 0, 0, 0, 1, 0, 0, 0, 1})
        {
        }

        public static MyMatrix3X3 RotationX(double alpha)
        {
            double s = Math.Sin(alpha * Math.PI / 180);
            double c = Math.Cos(alpha * Math.PI / 180);
            double[] vals = {1, 0, 0, 0, c, -s, 0, s, c};
            return new MyMatrix3X3(vals);
        }

        public static MyMatrix3X3 RotationY(double alpha)
        {
            double s = Math.Sin(alpha * Math.PI / 180);
            double c = Math.Cos(alpha * Math.PI / 180);
            double[] vals = {c, 0, s, 0, 1, 0, -s, 0, c};
            return new MyMatrix3X3(vals);
        }

        public static MyMatrix3X3 RotationZ(double alpha)
        {
            double s = Math.Sin(alpha * Math.PI / 180);
            double c = Math.Cos(alpha * Math.PI / 180);
            double[] vals = {c, -s, 0, s, c, 0, 0, 0, 1};
            return new MyMatrix3X3(vals);
        }

        public double Get(int x, int y)
        {
            return _matrix[y * 3 + x];
        }

        public double Get(int i)
        {
            return _matrix[i];
        }

        public MyMatrix3X3 Mult(double value)
        {
            return new MyMatrix3X3(_matrix.Select(val => val * value).ToArray());
        }

        public MyMatrix3X3 Mult(MyMatrix3X3 m2)
        {
            double[] m = new double[9];
            m[0] = _matrix[0] * m2._matrix[0] + _matrix[1] * m2._matrix[3] + _matrix[2] * m2._matrix[6];
            m[1] = _matrix[0] * m2._matrix[1] + _matrix[1] * m2._matrix[4] + _matrix[2] * m2._matrix[7];
            m[2] = _matrix[0] * m2._matrix[2] + _matrix[1] * m2._matrix[5] + _matrix[2] * m2._matrix[8];
            m[3] = _matrix[3] * m2._matrix[0] + _matrix[4] * m2._matrix[3] + _matrix[5] * m2._matrix[6];
            m[4] = _matrix[3] * m2._matrix[1] + _matrix[4] * m2._matrix[4] + _matrix[5] * m2._matrix[7];
            m[5] = _matrix[3] * m2._matrix[2] + _matrix[4] * m2._matrix[5] + _matrix[5] * m2._matrix[8];
            m[6] = _matrix[6] * m2._matrix[0] + _matrix[7] * m2._matrix[3] + _matrix[8] * m2._matrix[6];
            m[7] = _matrix[6] * m2._matrix[1] + _matrix[7] * m2._matrix[4] + _matrix[8] * m2._matrix[7];
            m[8] = _matrix[6] * m2._matrix[2] + _matrix[7] * m2._matrix[5] + _matrix[8] * m2._matrix[8];
            return new MyMatrix3X3(m);
        }


        public void MultThis(MyMatrix3X3 m2)
        {
            double[] m = new double[9];
            m[0] = _matrix[0] * m2._matrix[0] + _matrix[1] * m2._matrix[3] + _matrix[2] * m2._matrix[6];
            m[1] = _matrix[0] * m2._matrix[1] + _matrix[1] * m2._matrix[4] + _matrix[2] * m2._matrix[7];
            m[2] = _matrix[0] * m2._matrix[2] + _matrix[1] * m2._matrix[5] + _matrix[2] * m2._matrix[8];
            m[3] = _matrix[3] * m2._matrix[0] + _matrix[4] * m2._matrix[3] + _matrix[5] * m2._matrix[6];
            m[4] = _matrix[3] * m2._matrix[1] + _matrix[4] * m2._matrix[4] + _matrix[5] * m2._matrix[7];
            m[5] = _matrix[3] * m2._matrix[2] + _matrix[4] * m2._matrix[5] + _matrix[5] * m2._matrix[8];
            m[6] = _matrix[6] * m2._matrix[0] + _matrix[7] * m2._matrix[3] + _matrix[8] * m2._matrix[6];
            m[7] = _matrix[6] * m2._matrix[1] + _matrix[7] * m2._matrix[4] + _matrix[8] * m2._matrix[7];
            m[8] = _matrix[6] * m2._matrix[2] + _matrix[7] * m2._matrix[5] + _matrix[8] * m2._matrix[8];
            _matrix = m;
            _invertedMatrix = null;
        }

        public MyMatrix3X3 Inverted()
        {
            if (_invertedMatrix == null)
            {
                double[] m = new double[9];
                m[0] = _matrix[4] * _matrix[8] - _matrix[5] * _matrix[7];
                m[1] = _matrix[2] * _matrix[7] - _matrix[1] * _matrix[8];
                m[2] = _matrix[1] * _matrix[5] - _matrix[2] * _matrix[4];
                m[3] = _matrix[5] * _matrix[6] - _matrix[3] * _matrix[8];
                m[4] = _matrix[0] * _matrix[8] - _matrix[2] * _matrix[6];
                m[5] = _matrix[2] * _matrix[3] - _matrix[0] * _matrix[5];
                m[6] = _matrix[3] * _matrix[7] - _matrix[4] * _matrix[6];
                m[7] = _matrix[1] * _matrix[6] - _matrix[0] * _matrix[7];
                m[8] = _matrix[0] * _matrix[4] - _matrix[1] * _matrix[3];
                double det = _matrix[0] * m[0] + _matrix[1] * m[3] + _matrix[2] * m[6];
                if (det == 0) throw new ArithmeticException("/ by zero");
                for (int i = 0; i < 9; i++)
                    m[i] = m[i] / det;
                _invertedMatrix = new MyMatrix3X3(m);
            }
            return _invertedMatrix;
        }

        public void TransposeThis()
        {
            Swap(3, 1);
            Swap(6, 2);
            Swap(7, 5);
        }

        private void Swap(int index1, int index2)
        {
            double temp = _matrix[index1];
            _matrix[index1] = _matrix[index2];
            _matrix[index2] = temp;
        }

        public double Det()
        {
            double a = _matrix[4] * _matrix[8] - _matrix[5] * _matrix[7];
            double b = _matrix[5] * _matrix[6] - _matrix[3] * _matrix[8];
            double c = _matrix[3] * _matrix[7] - _matrix[4] * _matrix[6];
            return _matrix[0] * a + _matrix[1] * b + _matrix[2] * c;
        }

        public MyMatrix3X3 Normalized()
        {
            double scale = 1/_matrix[8];
            return Mult(scale);
        }

        public override String ToString()
        {
            String s = "";
            s += "[" + _matrix[0] + ", " + _matrix[1] + ", " + _matrix[2] + "]\n";
            s += "[" + _matrix[3] + ", " + _matrix[4] + ", " + _matrix[5] + "]\n";
            s += "[" + _matrix[6] + ", " + _matrix[7] + ", " + _matrix[8] + "]";
            return s;
        }

        public static MyMatrix3X3 GetTransformation(MyQuadrilateral q1, MyQuadrilateral q2)
        {
            double[] matrixTable =
            {
                q1.GetX(0), q1.GetY(0), 1, 0, 0, 0, -q2.GetX(0) * q1.GetX(0), -q2.GetX(0) * q1.GetY(0),
                q1.GetX(1), q1.GetY(1), 1, 0, 0, 0, -q2.GetX(1) * q1.GetX(1), -q2.GetX(1) * q1.GetY(1),
                q1.GetX(2), q1.GetY(2), 1, 0, 0, 0, -q2.GetX(2) * q1.GetX(2), -q2.GetX(2) * q1.GetY(2),
                q1.GetX(3), q1.GetY(3), 1, 0, 0, 0, -q2.GetX(3) * q1.GetX(3), -q2.GetX(3) * q1.GetY(3),
                0, 0, 0, q1.GetX(0), q1.GetY(0), 1, -q2.GetY(0) * q1.GetX(0), -q2.GetY(0) * q1.GetY(0),
                0, 0, 0, q1.GetX(1), q1.GetY(1), 1, -q2.GetY(1) * q1.GetX(1), -q2.GetY(1) * q1.GetY(1),
                0, 0, 0, q1.GetX(2), q1.GetY(2), 1, -q2.GetY(2) * q1.GetX(2), -q2.GetY(2) * q1.GetY(2),
                0, 0, 0, q1.GetX(3), q1.GetY(3), 1, -q2.GetY(3) * q1.GetX(3), -q2.GetY(3) * q1.GetY(3)
            };
            double[] vectorTable =
                {q2.GetX(0), q2.GetX(1), q2.GetX(2), q2.GetX(3), q2.GetY(0), q2.GetY(1), q2.GetY(2), q2.GetY(3)};
            MyMatrix myMatrix = new MyMatrix(8, 8, matrixTable);
            MyMatrix vector = new MyMatrix(8, 1, vectorTable);
            MyMatrix transformation = myMatrix.Inverse().Mul(vector);
            double[] transformationTable = new double[9];
            for (int i = 1; i <= 8; i++)
                transformationTable[i - 1] = transformation.GetElement(i, 1);
            transformationTable[8] = 1;
            MyMatrix3X3 solution = new MyMatrix3X3(transformationTable);
            solution.TransposeThis();
            return solution;
        }

        public Matrix<double> ToMat()
        {
            Matrix<double> matrix = new Matrix<double>(3, 3);
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    matrix[y, x] = Get(x, y);
                }
            }
            return matrix;
        }

        public Matrix3D ToMatrix3D() 
        {
 

            Matrix3D matrix3D = Matrix3D.Identity;
            matrix3D.M11 = Get(0, 0);
            matrix3D.M12 = Get(0, 1);
//            matrix3D.M13 = Get(0, 2);

            matrix3D.M21 = Get(1, 0);
            matrix3D.M22 = Get(1, 1);
//            matrix3D.M33 = Get(1, 2);

//            matrix3D.M31 = Get(2, 0);
//            matrix3D.M32 = Get(2, 1);
//            matrix3D.M33 = Get(2, 2);
            return matrix3D;
        }
    }
}