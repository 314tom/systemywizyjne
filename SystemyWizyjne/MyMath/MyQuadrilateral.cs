﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV.Structure;

namespace SystemyWizyjne.MyMath
{
    public class MyQuadrilateral
    {
        public List<Point> Points { get; }
        private int _rotation = 0;
        private double _a;
        private double _b;
        private double _c;
        private double _d;
        private double _p;
        private double _q;
        public double P => Math.Sqrt(_p);
        public double A => Math.Sqrt(_a);
        public double B => Math.Sqrt(_b);
        public double C => Math.Sqrt(_c);
        public double D => Math.Sqrt(_d);
        public double Q => Math.Sqrt(_q);

        public MyQuadrilateral(int x, int y, int width, int height)
        {
            Points = new List<Point>
            {
                new Point(x, y),
                new Point(x, y + height),
                new Point(x + width, y + height),
                new Point(x + width, y)
            };
            SetLengths();
        }

        public MyQuadrilateral(Point a, Point b, Point c, Point d)
        {
            Points = new List<Point> {a, b, c, d};
            SortPoints();
            SetLengths();
        }

        public static MyQuadrilateral Average(List<MyQuadrilateral> quadrilaterals)
        {
            var a = quadrilaterals.Select(q => q.Points[0]).ToList().AveragePoint();
            var b = quadrilaterals.Select(q => q.Points[1]).ToList().AveragePoint();
            var c = quadrilaterals.Select(q => q.Points[2]).ToList().AveragePoint();
            var d = quadrilaterals.Select(q => q.Points[3]).ToList().AveragePoint();
            return new MyQuadrilateral(a, b, c, d);
        }

        private void SetLengths()
        {
            _a = Dist(Points[0], Points[1]);
            _b = Dist(Points[1], Points[2]);
            _c = Dist(Points[2], Points[3]);
            _d = Dist(Points[3], Points[0]);
            _p = Dist(Points[0], Points[2]);
            _q = Dist(Points[1], Points[3]);
        }

        public void SetPoint(int index, Point point)
        {
            Points[index] = new Point(point.X, point.Y);
        }

        public void SetPoint(int index, int x, int y)
        {
            Points[index] = new Point(x, y);
        }

        public int GetX(int index)
        {
            return (int) Points[index].X;
        }

        public int GetY(int index)
        {
            return (int) Points[index].Y;
        }

        public Point GetPoint(int index)
        {
            return Points[index];
        }

        public double Area()
        {
            return 0.25 * Math.Sqrt(4 * _p * _q - Math.Pow(_a + _c - _b - _d, 2));
        }

        public override string ToString()
        {
            return Utils.ToString1(Points.ToList());
        }

        private double Dist(Point p1, Point p2)
        {
            return (Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public void SortPoints()
        {
            int max = Points.Min(p => p.X + p.Y);
            int index = Points.FindIndex(p => p.X + p.Y == max);
            List<Point> temp = new List<Point>(Points);
            Points.Clear();
            Point next = temp[(index + 1) % 4];
            Point prev = temp[(index + 3) % 4];
            int direction = -1;
            if (next.X < prev.X)
                direction *= -1;
            for (int i = 0; i < 4; i++)
            {
                Points.Add(temp[(i * direction + index + 4) % 4]);
            }
        }

        public Point Center()
        {
            return new Point((int) Points.Select(p => p.X).Average(), (int) Points.Select(p => p.Y).Average());
        }

        public int GetMaxXDiff()
        {
            int currentMax = Int32.MinValue;
            int currentMin = Int32.MaxValue;
            foreach (Point point in Points)
            {
                if (point.X > currentMax)
                    currentMax = point.X;
                if (point.X < currentMin)
                    currentMin = point.X;
            }
            return currentMax - currentMin;
        }

        public int GetMaxYDiff()
        {
            int currentMax = Int32.MinValue;
            int currentMin = Int32.MaxValue;
            foreach (Point point in Points)
            {
                if (point.Y > currentMax)
                    currentMax = point.Y;
                if (point.Y < currentMin)
                    currentMin = point.Y;
            }
            return currentMax - currentMin;
        }

        public List<LineSegment2D> GetLines()
        {
            List<LineSegment2D> lines = new List<LineSegment2D>();
            for (int i = 0; i < 4; i++)
            {
                lines.Add(new LineSegment2D(Points[i], Points[(i + 1) % 4]));
            }
            return lines;
        }

        public PointF[] GetPointsF()
        {
            return Enumerable.Range(_rotation, 4).Select(i => Points[(i) % 4]).Reverse().Select(p => new PointF(p.X, p.Y)).ToArray();
        }

        public void SetRotation(int index)
        {
            _rotation = index;
        }
    }
}