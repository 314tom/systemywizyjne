﻿using System;

namespace SystemyWizyjne.MyMath
{
    public class MyMatrix
    {

        private readonly int _row; //matrix rows number
        private int _col; //matrix columns number
        private double[] _elements; //0, 1 ... n -> next row...

        /**
         *
         * @param m rows
         * @param n columns
         * @param elem elements
         */
        public MyMatrix(int m, int n, double[] elem)
        {
            if (m * n == elem.Length)
            {
                _row = m;
                _col = n;
                _elements = elem;
            }
            else
            {
                throw new Exception("Wrong matrix format.");
            }
        }

        public int HowManyColumns()
        {
            return _col;
        }

        public int HowManyRows()
        {
            return _row;
        }

        /**
         *
         * @param i rows
         * @param j columns
         * @return
         */
        public double GetElement(int i, int j)
        {
            return _elements[(i - 1) * _col + j - 1];
        }

        /**
         *
         * @param element index
         * @return [0]-row [1]-columm
         */
        public int[] GetIndex(int element)
        {
            int[] index = new int[2];
            index[0] = (element / _col) + 1;
            index[1] = (element % _col) + 1;
            return index;
        }

        public MyMatrix Mul(MyMatrix b)
        {
            double[] newMatrix = new double[_row * b.HowManyColumns()];

            if (_col == b.HowManyRows())
            {
                for (int i = 0; i < newMatrix.Length; i++)
                {
                    double tmp = 0;
                    for (int I = 0; I < _col; I++)
                    {
                        int ai = i / (b.HowManyColumns()) + 1;
                        int aj = I + 1;
                        int bi = I + 1;
                        int bj = i % (b.HowManyColumns()) + 1;
                        tmp += this.GetElement(ai, aj) * b.GetElement(bi, bj);
                    }
                    newMatrix[i] = tmp;
                }
                return new MyMatrix(_row, b.HowManyColumns(), newMatrix);
            }
            else
            {
                throw new Exception("Number of rows B matrix and number of columns in A matrix are different");
            }
        }

        public MyMatrix Mul(double x)
        {
            double[] newMatrix = new double[_elements.Length];

            for (int i = 0; i < _elements.Length; i++)
            {
                newMatrix[i] = _elements[i] * x;
            }

            return new MyMatrix(_row, _col, newMatrix);
        }

        public MyMatrix Inverse()
        {
            double det = this.det();
            if (det != 0)
            {
                MyMatrix d = this.Complement();
                d = d.Transposition();
                return d.Mul(1 / det);
            }
            else
            {
                throw new Exception("det=0!");
            }
        }

        public double det()
        {
            if (_col == _row)
            {
                if (_col > 1)
                {
                    return Det(this, _col);
                }
                else
                {
                    return _elements[0];
                }
            }
            else
            {
                throw new Exception("Matrix is not square");
            }
        }

        private double Det(MyMatrix myMatrix, int m)
        {
            double det = 0;
            if (myMatrix.HowManyColumns() > 1)
            {
                for (int i = 0; i < m; i++)
                {
                    int X = myMatrix.GetIndex(i)[0]; //row
                    int Y = myMatrix.GetIndex(i)[1]; //column
                    double[] tmp = new double[(m - 1) * (m - 1)];
                    int next = 0;
                    for (int x = 1; x <= m; x++)
                    {
                        if (x != X)
                        {
                            for (int y = 1; y <= m; y++)
                            {
                                if (y != Y)
                                {
                                    tmp[next++] = myMatrix.GetElement(x, y);
                                }
                            }
                        }
                    }
                    det += myMatrix.GetElement(1, Y) * Math.Pow((-1), (X + Y)) * Det(new MyMatrix(m - 1, m - 1, tmp), m - 1);
                }
            }
            else
            {
                det = myMatrix.GetElement(1, 1);
            }
            return det;
        }

        public MyMatrix Complement()
        {
            if (_col == _row)
            {
                double[] cMatrix = new double[_elements.Length];
                for (int i = 0; i < _elements.Length; i++)
                {
                    int X = this.GetIndex(i)[0];
                    int Y = this.GetIndex(i)[1];
                    int one = 1;
                    if ((X + Y) % 2 == 1)
                    {
                        one = (-1);
                    }

                    double[] tmp = new double[(_col - 1) * (_col - 1)];
                    int next = 0;
                    for (int x = 1; x <= _row; x++)
                    {
                        if (x != X)
                        {
                            for (int y = 1; y <= _col; y++)
                            {
                                if (y != Y)
                                {
                                    tmp[next++] = this.GetElement(x, y);
                                }
                            }
                        }
                    }

                    cMatrix[i] = one * Det(new MyMatrix(_col - 1, _col - 1, tmp), _col - 1);
                }
                return new MyMatrix(_col, _row, cMatrix);
            }
            else
            {
                throw new Exception("Matrix is not square");
            }
        }

        public MyMatrix Transposition()
        {
            double[] tMatrix = new double[_elements.Length];
            int next = 0;
            for (int n = 1; n <= _col; n++)
            {
                for (int m = 1; m <= _row; m++)
                {
                    tMatrix[next++] = this.GetElement(m, n);
                }
            }
            return new MyMatrix(_col, _row, tMatrix);
        }

        public override String ToString()
        {
            String matrix = "{";
            for (int n = 0; n < _elements.Length; n++)
            {
                matrix += _elements[n] + "";
                if (n < _elements.Length - 1)
                {
                    matrix += "; ";
                }
            }
            matrix += "}";
            return matrix;
        }
    }
}
